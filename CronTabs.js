const cron = require("node-cron");
var moment = require('moment');
let PromisesHelper = require('./helpers/promisesHelper');
let EmailServices = require('./gateway/emailServices');
let SaleServices = require('./gateway/saleServices');
let SfInvoiceController = require('./Controllers/sfInvoiceController');
let LogHelper = require('./helpers/logHelper');
let ResponseHelper = require('./gateway/responseHelper');
let SalesHelper = require('./helpers/salesHelper');

class CronTabs {

  allCrons() {

    //send email from xml
    cron.schedule("*/5 * * * *", async function() {

      try{
        LogHelper.logResponse("Send email cron started");
        let xmlFiles = await PromisesHelper.getAllXmlFiles();
        for(let xmlFile of xmlFiles){
          EmailServices.sendEmailFromXml(xmlFile);
        }
      }catch(error){
        LogHelper.logResponse("Error in send invoice mail \n" + error.message);
      }

    });//send email from xml

    //SF invoices in db
    cron.schedule("0 0 */2 * * *", async function() {

      try{
        LogHelper.logResponse("SF DB cron started");
        let sfInvoices = await SfInvoiceController.getAllRequests();
        // const invoicesMap = {};
        sfInvoices.forEach(async (invoice) => {

            let request = JSON.parse(invoice.request);
            let filePrefix = SalesHelper.returnInvoiceFilePrefix(request.warehouseCode);
            let filetoday = moment(request.orderDate).format('YYYYMMDD');
            let response = await SaleServices.generateInvoice(request);
            let content = '';
            if (response.status === 200) {
              let fixResponse = ResponseHelper.createInvoiceResponse(response.data);
              let invoiceNumber = fixResponse['Key'][0]['Number'][0];
              let file_link = SalesHelper.generateFileName(filePrefix,invoiceNumber, filetoday);
              SfInvoiceController.removeRequest(invoice._id);
              //sent email to customer after 1 minute
              setTimeout(async function(){
                let emailSent =  await EmailServices.sendURLEmail(request.email, file_link);
              }, 60000);

            } else {
              content = ResponseHelper.createErrorResponse(response.data);
              let requestStringify = JSON.stringify(request);
              let emailSent =  await EmailServices.sendEmailToHQ(request.orderNumber, requestStringify, content);
            }
        });
      }catch(error){
        LogHelper.logResponse("Error in SF db invoice mail \n" + error.message);
      }

    });//SF invoices in db


  }//allCrons

}//CronTabs


module.exports = new CronTabs();
