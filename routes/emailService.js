let PromisesHelper = require('../helpers/promisesHelper');
let EmailServices = require('../gateway/emailServices');

const { PMI_PREFIX, INVOICES_PUT_PATH } = require('../constants');

module.exports = function (app, fns, magento) {

  app.get(PMI_PREFIX + '/sendEmail', [], async (req, res) => {

    try{
      let xmlFiles = await PromisesHelper.getAllXmlFiles();

      for(let xmlFile of xmlFiles){
        EmailServices.sendEmailFromXml(xmlFile);
      }
      fns.apiResponse(res, 200, true, "Email sent successfully");
    }catch(error){
      fns.apiResponse(res, 500, true, error.message);
    }
  });//sendEmail
}
