let AramexServices = require('../gateway/aramexServices');
let PromisesHelper = require('../helpers/promisesHelper');
var _ = require('lodash');
const { PUT_PATH, PMI_PREFIX } = require('../constants');

module.exports = function (app, fns, magento) {

  app.get(PMI_PREFIX + '/callAramex', [], async (req, res) => {
    try {
      let filePath = PUT_PATH + 'MAG_DELIVERYNOTICE_SAMPLE.csv';
      let allSales = await PromisesHelper.readCSVFile(filePath);
      let uniqueSale = _.uniqBy(allSales, 'TransactionID');
      for (let sale of uniqueSale) {
        let response = await AramexServices.importASN(sale, allSales);
      }
      fns.apiResponse(res, 200, true, "api run")
    } catch (error) {
      fns.apiResponse(res, 500, false, error.message)
    }

    return false;

  });

}
