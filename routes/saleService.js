let saleServices = require('../gateway/saleServices');
let EmailServices = require('../gateway/emailServices');
let SfInvoiceController = require('../Controllers/sfInvoiceController');
const { check, validationResult } = require('express-validator/check');
let responseHelper = require('../gateway/responseHelper');
let salesHelper = require('../helpers/salesHelper');
var {receivestockArray, generateTransactionValidation,
    updateTransactionValidation,
    updatePaymentValidation, generateReplacementValidation} = require('../helpers/validationArray');
var moment = require('moment');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { PMI_PREFIX, REPLACEMENT_PREFIX, RETURN_PREFIX, GET_PATH, CEGID } = require('../constants');

module.exports = function(app, fns, magento) {

  app.post(PMI_PREFIX+'/generateInvoice', generateTransactionValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }

    let isReturnOrder = salesHelper.isReturnOrder(req.body);

    if(isReturnOrder){
      if(!salesHelper.isValidReturnOrderNumber(req.body.orderNumber)){
        fns.apiResponse(res, 500, false, 'For the Return, order number must start with ' + RETURN_PREFIX);
        return;
      }

      // if(!salesHelper.checkReturnPricesAreValid(req.body)){
      //   fns.apiResponse(res, 500, false, 'For the Return, all the prices must be greater than 0.');
      //   return;
      // }
    }

    let warehouseCode = salesHelper.mapWarehouseCode(req.body.warehouseCode);

    if(!warehouseCode){
      fns.apiResponse(res, 500, false, 'Invalid Warehouse code. ');
      return;
    }else
    req.body.warehouseCode = warehouseCode.cegid_code;
    req.body.cegidStoreCode = warehouseCode.cegid_store_code;
    req.body.cegidOrderOrigin = CEGID.ORIGIN;
    req.body.cegidOrderType =  CEGID.ORDER_TYPE;

    //let today = moment().format('YYYY-MM-DD');
    let filetoday = moment(req.body.orderDate).format('YYYYMMDD');

    let response = await saleServices.generateInvoice(req.body);
    let content = '';
    if (response.status === 200) {
      let fixResponse = responseHelper.createInvoiceResponse(response.data);
      let invoiceNumber = fixResponse['Key'][0]['Number'][0];
      let file_link = salesHelper.generateFileName(warehouseCode.file_prefix,invoiceNumber, filetoday);
      content = {
        invoiceNumber: fixResponse['Key'][0]['Number'][0],
        fileLink: file_link,
        orderDate: req.body.orderDate,
        orderNumber: req.body.orderNumber,
        message: 'Please access the file in the link after 1 minute'
      };
    } else if (response.status === 401) {
      content = 'You are not authorized to access.';
    } else {
      content = responseHelper.createErrorResponse(response.data);
    }
    fns.apiResponse(res, response.status, true, content);
  });//generate invoice

  app.post(PMI_PREFIX+'/generateReplacement', generateReplacementValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }

    if(!salesHelper.isValidReplacement(req.body)){
      fns.apiResponse(res, 500, false, 'For the Replacement, order number must start with ' + REPLACEMENT_PREFIX);
      return;
    }

    //let today = moment().format('YYYY-MM-DD');
    let filetoday = moment(req.body.orderDate).format('YYYYMMDD');
    //console.log(filetoday);return false;
    let response = await saleServices.generateReplacement(req.body);

    let content = '';
    if (response.status === 200) {
      let fixResponse = responseHelper.createInvoiceResponse(response.data);
      let invoiceNumber = fixResponse['Key'][0]['Number'][0];
      let file_link = salesHelper.generateFileName(invoiceNumber, filetoday);
      content = {
        invoiceNumber: fixResponse['Key'][0]['Number'][0],
        fileLink: file_link,
        orderDate: req.body.orderDate,
        orderNumber: req.body.orderNumber,
        message: 'Please access the file in the link after 1 minute'
      };
    } else if (response.status === 401) {
      content = 'You are not authorized to access.';
    } else {
      content = responseHelper.createErrorResponse(response.data);
    }
    fns.apiResponse(res, response.status, true, content);
  });//generate invoice

  app.post(PMI_PREFIX+'/updatePayment', updatePaymentValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }
    let today = moment().format('YYYY-MM-DD');
    let response = await saleServices.updatePayment(req.body, today);

    let content = '';
    if (response.status === 200) {
      content = {
        message: 'Payment has been successfully updated'
      };
    } else if (response.status === 401) {
      content = 'You are not authorized to access.';
    } else {
      content = responseHelper.createErrorResponse(response.data);
    }
    fns.apiResponse(res, response.status, true, content);
  });// update payment

  app.post(PMI_PREFIX+'/updateInvoice', updateTransactionValidation, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }
    let warehouseCode = salesHelper.mapWarehouseCode(req.body.warehouseCode);

    if(!warehouseCode){
      fns.apiResponse(res, 500, false, 'Invalid Warehouse code. ');
      return;
    }

    let fileDate = moment(req.body.orderDate).format('YYYYMMDD');
    let file_link = salesHelper.generateFileName(warehouseCode.file_prefix, req.body.invoiceNumber, fileDate);
    content = {
      invoiceNumber: req.body.invoiceNumber,
      fileLink: file_link,
      orderDate: req.body.orderDate,
      orderNumber: req.body.orderNumber,
      message: 'Invoice is regenerated.'
    };
    fns.apiResponse(res, 200, true, content);
  });

  app.post(PMI_PREFIX+'/cancelInvoice', [], async (req, res) => {

    let response = await saleServices.cancelOrder(req.body);

    let content = '';
    if (response.status === 200) {
      content = {
        message: "Order has been successfully cancelled."
      };
    } else if (response.status === 401) {
      content = "You are not authorized to access.";
    } else {
      content = responseHelper.createErrorResponse(response.data);
    }
    fns.apiResponse(res, response.status, true, content);
  });

  app.post(PMI_PREFIX+'/receivestock', receivestockArray, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }

    const FILE_NAME = GET_PATH + 'receiveStock_' + Date.now() + ".csv";

    const csvWriter = createCsvWriter({
      path: FILE_NAME,
      header: [
        { id: 'identifier', title: 'Identifier' },
        { id: 'store_code', title: 'Store Code' },
        { id: 'internal_reference', title: 'Internal Reference' },
        { id: 'receipt_date', title: 'Delivery Date' },
        { id: 'document_date', title: 'Document Date' },
        { id: 'barcode', title: 'barcode' },
        { id: 'codentifier', title: 'Codentifier' },
        { id: 'quantity', title: 'Quantity' },
        { id: 'batchnumber', title: 'Batch Number' },
      ]
    });
    let request = req.body;

    let identifier = salesHelper.isPOReceiveStock(req.body.internal_reference) ? 'BLTC1R' : 'BLFC1R' ;

    let headerObj = {
      identifier: identifier,
      store_code: request.store_code,
      internal_reference: request.internal_reference,
      receipt_date: request.receipt_date,
      document_date: request.document_date
    };

    const data = [];
    for (let item of request.items) {
      data.push({
        ...headerObj,
        barcode: item.barcode,
        quantity: item.quantity,
        codentifier: item.codentifier,
        batchnumber: item.batchnumber
      });
    }

    csvWriter
      .writeRecords(data)
      .then(() => fns.apiResponse(res, 200, true, "Receive stock file is generated successfully."))
      .catch((error) => fns.apiResponse(res, 500, false, error.message));

  });

  app.get(PMI_PREFIX+'/helloWorld', [], async (req, res) => {
    let response = await saleServices.callHelloWorld();
    fns.apiResponse(res, 200, true, response)
  });

  app.post(PMI_PREFIX+'/sfGenerateInvoice', generateTransactionValidation, async (req, res) => {


    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      fns.apiResponse(res, 422, false, errors.array());
      return;
    }

    let warehouseCode = salesHelper.mapWarehouseCode(req.body.warehouseCode);

    if(!warehouseCode){
      fns.apiResponse(res, 500, false, 'Invalid Warehouse code. ');
      return;
    }else

    req.body.warehouseCode = warehouseCode.cegid_code
    req.body.warehouseCode = warehouseCode.cegid_code;
    req.body.cegidStoreCode = warehouseCode.cegid_store_code;
    req.body.cegidOrderOrigin = CEGID.SF_ORIGIN;
    req.body.cegidOrderType =  CEGID.SF_ORDER_TYPE;

    //let today = moment().format('YYYY-MM-DD');
    let filetoday = moment(req.body.orderDate).format('YYYYMMDD');

    let response = await saleServices.generateInvoice(req.body);
    let content = '';
    if (response.status === 200) {
      let fixResponse = responseHelper.createInvoiceResponse(response.data);
      let invoiceNumber = fixResponse['Key'][0]['Number'][0];
      let file_link = salesHelper.generateFileName(warehouseCode.file_prefix,invoiceNumber, filetoday);
      content = {
        invoiceNumber: fixResponse['Key'][0]['Number'][0],
        fileLink: file_link,
        orderDate: req.body.orderDate,
        orderNumber: req.body.orderNumber,
        message: 'Please access the file in the link after 1 minute'
      };
      //sent email to customer after 1 minute
      setTimeout(async function(){
        let emailSent =  await EmailServices.sendURLEmail(req.body.email, file_link);
      }, 60000);

    } else if (response.status === 401) {
      content = 'You are not authorized to access.';
    } else {
      content = responseHelper.createErrorResponse(response.data);
      request = JSON.stringify(req.body);
      //send email to HQ
      let emailSent =  EmailServices.sendEmailToHQ(req.body.orderNumber, request, content);

      //store reqeust in database
      //only if the error is based on serial number
      if(content.includes("not available in inventory")){
        let sfInvoice = await SfInvoiceController.saveRequestData(req.body);
      }
    }
    fns.apiResponse(res, response.status, true, content);
  });//sales force generate invoice

};
