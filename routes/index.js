const { PMI_PREFIX } = require('../constants');
module.exports = function(app, fns) {
  /* RESPONSE  FUNCTION*/
  var apiResponse = function(res, status_code, status, content) {
    var response = {};
    if (status == false) {
      content = { code: status_code, message: content };
      response = {
        status: status,
        error: content
      };
    } else {
      response = {
        status: status,
        content: content
      };
    }
    res.status(status_code).json(response);
  };

  var fns = { apiResponse: apiResponse };

  /* ROUTE MIDDLEWARE*/
  app.use(function(req, res, next) {
    //console.log('Called: ', req.path);
    next();
  });

  //Sale Services
  var saleService = require('./saleService')(app, fns);

  var aramexService = require('./aramexService')(app, fns);

  var emailService = require('./emailService')(app, fns);



  /* DEFAULT ROUTE*/
  app.get(PMI_PREFIX + '/', function(req, res) {
	  //res.send('tes233 is working on IISNode!');
    fns.apiResponse(res, 404, false, 'Invalid end point');
  });
};
