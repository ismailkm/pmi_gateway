var express = require('express');
var app = express();
//import mongoose
let mongoose = require('mongoose');

var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
const { PORT } = require('./constants');
//routing goes to routes folder
var routes = require('./routes/index')(app);

var CronTabs = require('./CronTabs');
CronTabs.allCrons();

//connect to mongoose
const dbPath = 'mongodb://localhost/altavant';
const options = {useNewUrlParser: true, useUnifiedTopology: true}
const mongo = mongoose.connect(dbPath, options);
mongo.then(() => {
    console.log('mogodb connected');
}, error => {
    console.log(error, 'mogodb error');
})

app.listen(PORT);
