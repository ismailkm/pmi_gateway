module.exports = {
  PORT: '4003',
  PMI_PREFIX: '/pmi_gateway_qa',
  REPLACEMENT_PREFIX: 'REP',
  RETURN_PREFIX: 'RET',
  REPLACEMENT: 'replacement',
  SALE_TYPE: 'sale',
  GET_PATH: '../../../www/html/get_qa/',
  PUT_PATH: '../../../www/html/put_qa/',
  INVOICES_PUT_PATH: '../../../www/html/put_qa/invoices/',
  LOG_PATH: '../../../www/html/gt_log_qa/',
  CEGID:{
    CLIENT_URL: 'http://www.cegid.fr/Retail/1.0',
    BASE_URL: 'http://15.206.25.159/GULFTALEED_TEST/SaleDocumentService.svc',
    DATABASE_ID: 'GULFTALEED_TEST',
    DOMAIN: 'GULFTALEED_TEST',
    USER: 'USER_CONSULTING1',
    PASSWORD: 'Altavant@12',
    STORE_ID: 'KSAECM',
    CUSTOMER_ID: 'WALKIN01',
    ORIGIN: 'ECommerce',
    ORDER_TYPE: 'CustomerOrder',
    SF_ORIGIN: 'Shop',
    SF_ORDER_TYPE: 'Receipt',
    CURRENCY_ID: 'SAR',
    CANCELLATION_ID: 'CAN',
    SHIPPING_ID: '001',
    INVOICE_FILE_NAME: 'http://15.206.25.159/Invoices_QA/'
  },
  ARAMEX:{
    BASE_URL: 'https://cportal.infor.aramex.com/WS_EDI_TEST_V02/RestService_API/InBound/ImportASN',
    SSA_LOGIN: 'wspmi',
    SSA_PASSWORD: 'pass',
    FACILITY: 'WMWHSE1',
    STORE_KEY: 'PMIKSA'
  },
  FACILITY_CODE:[
    {
      'code': 'WMWHSE1',
      'store_key': 'KSAECM',
    },
    {
      'code': 'WMWHSE2',
      'store_key': '16A',
    }
  ],
  WAREHOUSE_CODES:[
    {
      'aramex_code': 'WMWHSE1',
      'cegid_code': 'KSAECM',
      'cegid_store_code': 'KSAECM',
      'file_prefix': 'e-com001',
      'city': 'riyadh'
    },
    {
      'aramex_code': 'WMWHSE2',
      'cegid_code': 'KSAJDH',
      'cegid_store_code': 'KSAJDH',
      'file_prefix': 'e-com002',
      'city': 'jeddah'
    },
    {
      'aramex_code': 'TK2A',
      'cegid_code': 'TK2A',
      'cegid_store_code': '- TKF',
      'file_prefix': 'click_&_collect_-_tamimi_king_fahad',
      'city': 'click & collect'
    },
    {
      'aramex_code': 'TN2A',
      'cegid_code': 'TN2A',
      'cegid_store_code': '- TNU',
      'file_prefix': 'click_&_collect_-_tamimi_nuzha',
      'city': 'click & collect'
    },
    {
      'aramex_code': 'TR2A',
      'cegid_code': 'TR2A',
      'cegid_store_code': '- TRA',
      'file_prefix': 'click_&_collect_-_tamimi_rawda',
      'city': 'click & collect'
    }
  ],
  SMTP_DETAILS: {
    host: "smtp-relay.sendinblue.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'spignard@altavantconsulting.com', // generated ethereal user
      pass: '97U5IZGOrWQbFsP4', // generated ethereal password
    },
    tls: {
      rejectUnauthorized: false
    }
  },
  FROM_EMAIL: 'spignard@altavantconsulting.com',
  HQ_EMAIL: 'jalibudbud@altavantconsulting.com',
  HQ_CC_EMAILS: ''
}
