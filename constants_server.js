module.exports = {
  PORT: '4001',
  PMI_PREFIX: '/pmi_gateway',
  REPLACEMENT_PREFIX: 'REP',
  RETURN_PREFIX: 'RET',
  REPLACEMENT: 'replacement',
  SALE_TYPE: 'sale',
  GET_PATH: '../../www/html/get/',
  PUT_PATH: '../../www/html/put/',
  INVOICES_PUT_PATH: '../../www/html/put/invoices/',
  LOG_PATH: '../../www/html/gt_log/',
  CEGID:{
    CLIENT_URL: 'http://www.cegid.fr/Retail/1.0',
    BASE_URL: 'http://15.206.25.159/GULFTALEED/SaleDocumentService.svc',
    DATABASE_ID: 'GULFTALEED',
    DOMAIN: 'GULFTALEED',
    USER: 'USER_CONSULTING1',
    PASSWORD: 'Altavant@12',
    STORE_ID: 'KSAECM',
    CUSTOMER_ID: 'SCAAE044',
    ORIGIN: 'ECommerce',
    ORDER_TYPE: 'CustomerOrder',
    SF_ORIGIN: 'Shop',
    SF_ORDER_TYPE: 'Receipt',
    CURRENCY_ID: 'SAR',
    CANCELLATION_ID: 'CAN',
    SHIPPING_ID: '001',
    INVOICE_FILE_NAME: 'http://15.206.25.159/invoices/'
  },
  ARAMEX:{
    BASE_URL: 'https://cportal.infor.aramex.com/WS_EDI_V02/RestService_API/InBound/ImportASN',
    SSA_LOGIN: 'wspmi_cg',
    SSA_PASSWORD: 'j98$h8as',
    FACILITY: 'WMWHSE8',
    STORE_KEY: 'IQOS'
  },
  FACILITY_CODE:[
    {
      'code': 'WMWHSE1',
      'store_key': 'KSAECM',
    },
    {
      'code': 'WMWHSE2',
      'store_key': '16A',
    }
  ],
  WAREHOUSE_CODES:[
    {
      'aramex_code': 'WMWHSE8',
      'cegid_code': 'KSAECM',
      'cegid_store_code': 'KSAECM',
      'file_prefix': 'e-com001',
      'city': 'riyadh'
    },
    {
      'aramex_code': 'WMWHSE2',
      'cegid_code': 'KSAJDH',
      'cegid_store_code': 'KSAJDH',
      'file_prefix': 'e-com002',
      'city': 'jeddah'
    },
    {
      "aramex_code": "- F01A",
      "cegid_code": "- F01A",
      "cegid_store_code": "- F018",
      "file_prefix": "click_&_collect_-_farm_ras_tanoura",
      "city": "click & collect"
    }, {
      "aramex_code": "- F02A",
      "cegid_code": "- F02A",
      "cegid_store_code": "- F027",
      "file_prefix": "click_&_collect_-_farm_qatif",
      "city": "click & collect"
    }, {
      "aramex_code": "- F030",
      "cegid_code": "- F030",
      "cegid_store_code": "- F030",
      "file_prefix": "click_&_collect_-_farm_khafji",
      "city": "click & collect"
    }, {
      "aramex_code": "- F035",
      "cegid_code": "- F035",
      "cegid_store_code": "- F035",
      "file_prefix": "click_&_collect_-_farm_khafji_035",
      "city": "click & collect"
    }, {
      "aramex_code": "- F04A",
      "cegid_code": "- F04A",
      "cegid_store_code": "- F048",
      "file_prefix": "click_&_collect_-_farm_arar",
      "city": "click & collect"
    }, {
      "aramex_code": "- F08A",
      "cegid_code": "- F08A",
      "cegid_store_code": "- F080",
      "file_prefix": "click_&_collect_-_farm_sakaka",
      "city": "click & collect"
    }, {
      "aramex_code": "- LHAA",
      "cegid_code": "- LHAA",
      "cegid_store_code": "- LHAL",
      "file_prefix": "click_&_collect_-_lulu_hail",
      "city": "click & collect"
    }, {
      "aramex_code": "- T10A",
      "cegid_code": "- T10A",
      "cegid_store_code": "- T105",
      "file_prefix": "click_&_collect_-_tamimi_khuzama_kh",
      "city": "click & collect"
    }, {
      "aramex_code": "- T117",
      "cegid_code": "- T117",
      "cegid_store_code": "- T117",
      "file_prefix": "click_&_collect_-_tamimi_khaleej_da",
      "city": "click & collect"
    }, {
      "aramex_code": "- T119",
      "cegid_code": "- T119",
      "cegid_store_code": "- T119",
      "file_prefix": "click_&_collect_-_tamimi_jubail",
      "city": "click & collect"
    }, {
      "aramex_code": "- T12A",
      "cegid_code": "- T12A",
      "cegid_store_code": "- T120",
      "file_prefix": "click_&_collect_-_tamimi_hassa",
      "city": "click & collect"
    }, {
      "aramex_code": "- T19B",
      "cegid_code": "- T19B",
      "cegid_store_code": "- T190",
      "file_prefix": "click_&_collect_-_tamimi_unaizah",
      "city": "click & collect"
    }, {
      "aramex_code": "- T19A",
      "cegid_code": "- T19A",
      "cegid_store_code": "- T195",
      "file_prefix": "click_&_collect_-_tamimi_hafr",
      "city": "click & collect"
    }, {
      "aramex_code": "- TNK",
      "cegid_code": "- TNK",
      "cegid_store_code": "- TNK",
      "file_prefix": "click_&_collect_-_tamimi_nakheel",
      "city": "click & collect"
    }, {
      "aramex_code": "Z01A",
      "cegid_code": "Z01A",
      "cegid_store_code": "- TNU",
      "file_prefix": "click_&_collect_-_tamimi_nuzha",
      "city": "click & collect"
    }, {
      "aramex_code": "W01A",
      "cegid_code": "W01A",
      "cegid_store_code": "- TRA",
      "file_prefix": "click_&_collect_-_tamimi_rawda",
      "city": "click & collect"
    }
  ],
  SMTP_DETAILS: {
    host: "smtp.office365.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'iqos@taleed.com.sa', // generated ethereal user
      pass: '#@!T@leed#2021', // generated ethereal password
    },
    tls: {
      rejectUnauthorized: true,
        ciphers:'SSLv3'
    }
  },
  FROM_EMAIL: 'iqos@taleed.com.sa',
  HQ_EMAIL: 'jalibudbud@altavantconsulting.com',
  HQ_CC_EMAILS: ''
}
