var nodemailer = require('nodemailer');
const fs = require("fs");

let PromisesHelper = require('../helpers/promisesHelper');
let LogHelper = require('../helpers/logHelper');

const { PMI_PREFIX, INVOICES_PUT_PATH, SMTP_DETAILS, FROM_EMAIL, HQ_EMAIL, HQ_CC_EMAILS } = require('../constants');

module.exports = {

  sendEmailFromXml: async function(xmlFile){
      let filesplit = xmlFile.split(".");
      let pdfFileName = filesplit[0]+".pdf";
      let pdfFile = INVOICES_PUT_PATH+pdfFileName;
      let xmlFilePath = INVOICES_PUT_PATH + xmlFile;
      let pdfFileExists = await PromisesHelper.fileExists(pdfFile);
      if(pdfFileExists){
        let xmlData = await PromisesHelper.readXmlFile(xmlFilePath);
        try{
          let email = await this.sendEmail(pdfFile, xmlData);
          PromisesHelper.deleteFile(xmlFilePath)
          PromisesHelper.deleteFile(pdfFile)
          LogHelper.logResponse(pdfFileName + " sent to the customer");
        }catch(error){
          LogHelper.logResponse("email not send \n" + error.message);
        }
        //console.log(email.messageId);
      }else{
        LogHelper.logResponse(pdfFileName + " not found on server");
      }
  },//sendEmailFromXml
  sendEmail: function(pdfFile, xmlData){
     let transporter = nodemailer.createTransport(SMTP_DETAILS);
     return transporter.sendMail({
                    from: FROM_EMAIL,
                    to: xmlData.recipient,
                    subject: 'Invoice for your purchase on IQOS.COM',
                    text: ' ',
                    html: '<b>&nbsp;</b>',
                    attachments: [
                        {   // utf-8 string as an attachment
                            filename: 'invoice.pdf',
                            content: fs.createReadStream( pdfFile )
                        },
                    ]
                  });
  },//sendEmail
  sendURLEmail: function(toEmail, fileUrl){
     let transporter = nodemailer.createTransport(SMTP_DETAILS);
     return transporter.sendMail({
                    from: FROM_EMAIL,
                    to: toEmail,
                    subject: 'Invoice for your purchase on IQOS.COM',
                    text: ' ',
                    html: '<b>&nbsp;</b>',
                    attachments: [
                        {   // utf-8 string as an attachment
                            filename: 'invoice.pdf',
                            path: fileUrl
                        },
                    ]
                  });
  },//sendURLEmail
  sendEmailToHQ: function(orderNumber, request, response){
     let transporter = nodemailer.createTransport(SMTP_DETAILS);
     return transporter.sendMail({
                    from: FROM_EMAIL,
                    to: HQ_EMAIL,
                    cc: HQ_CC_EMAILS,
                    subject: 'Failed invoice generation for order number ' + orderNumber,
                    text: ' ',
                    html: '<p><strong>Request</strong><br />'+request+'</p><p><strong>Response</strong><br />'+response+'</p>',
                  });
  }//sendEmailToHQ

}//end of module
