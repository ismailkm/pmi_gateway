const fetch = require('node-fetch');
var xml2js = require('xml2js');
const { base64encode } = require('nodejs-base64');
let LogHelper = require('../helpers/logHelper');
let salesHelper = require('../helpers/salesHelper');
const { CEGID: { CLIENT_URL, BASE_URL, DATABASE_ID, DOMAIN, USER, PASSWORD,
                STORE_ID, CUSTOMER_ID, ORIGIN, ORDER_TYPE, CURRENCY_ID,
                CANCELLATION_ID, SHIPPING_ID } } = require('../constants');

const ACTION_URL = CLIENT_URL + '/ISaleDocumentService';

const CREATE_ACTION = ACTION_URL + '/Create';
const UPDATEPAYMENT_ACTION = ACTION_URL + '/UpdatePayments';
const UPDATEHEADER_ACTION = ACTION_URL + '/UpdateHeader';
const AUTH_CODE = 'Basic ' + base64encode(DOMAIN + '\\'+ USER +':'+ PASSWORD); // user name Y2_DEMO\ENG

let requestHeader = {
  'Content-Type': 'text/xml; charset=utf-8',
  Authorization: AUTH_CODE
};

let requestObject = {
  'soapenv:Envelope': {
    $: {
      'xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
      'xmlns:ns': CLIENT_URL
    },
    'soapenv:Header': {},
    'soapenv:Body': {}
  }
};

var builder = new xml2js.Builder();

module.exports = {

  generateInvoice: function(requestData) {
    requestObject['soapenv:Envelope'][
      'soapenv:Body'
    ] = salesHelper.returnCreateInvioceRequestXML(requestData);

    var xml = builder.buildObject(requestObject);

    requestHeader['SOAPAction'] = CREATE_ACTION;
    return fetch(BASE_URL, {
      method: 'POST',
      headers: requestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
    });
  }, //end of generateInvoice


  updatePayment: function(requestData, today) {
    requestObject['soapenv:Envelope'][
      'soapenv:Body'
    ] = this.returnUpdatePaymentRequestXML(requestData, today);
    var xml = builder.buildObject(requestObject);
    requestHeader['SOAPAction'] = UPDATEPAYMENT_ACTION;
    return fetch(BASE_URL, {
      method: 'POST',
      headers: requestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
    });
  }, //end of updatePayment

   //PAYMENT UPDATE REQUEST
  returnUpdatePaymentRequestXML: function(
    { orderNumber, totalAmount, paid, paymentType }, today
  ) {
    let requestObj = {
      'ns:UpdatePayments': {
          'ns:updatePaymentsRequest':{
            'ns:Identifier': {
              'ns:Reference': {
                'ns:CustomerId': { _: CUSTOMER_ID},
                'ns:InternalReference': { _: orderNumber},
                'ns:Type': { _: ORDER_TYPE},
              }
            },
            'ns:Payments': {
              'ns:Update_Payment': {
                'ns:Amount': { _: totalAmount },
                'ns:CurrencyId': { _: CURRENCY_ID },
                'ns:DueDate': { _: today },
                'ns:Id': { _: 1 },
                'ns:MethodId': { _: paymentType },
                'ns:IsReceivedPayment': { _: paid }
              }
            },
            'ns:clientContext': {
              'ns:DatabaseId': { _: DATABASE_ID }
            },
          }
        }
    };
    return requestObj;
  },
  //END PAYMENT UPDATE REQUEST

  updateInvoice: function(requestData) {
    requestObject['soapenv:Envelope'][
      'soapenv:Body'
    ] = this.returnUpdateInvoiceRequestXML(requestData);
    var xml = builder.buildObject(requestObject);

    requestHeader['SOAPAction'] = UPDATEHEADER_ACTION;
    return fetch(BASE_URL, {
      method: 'POST',
      headers: requestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
    });
  }, //end of updateInvoice

   //INVOICE UPDATE REQUEST
  returnUpdateInvoiceRequestXML: function(
    { orderNumber }
  ){
    let requestObj = {
      'ns:UpdateHeader': {
          'ns:updateHeaderRequest':{
            'ns:FollowedReference': {_: 'update' },
            'ns:Identifier': {
              'ns:Reference': {
                'ns:CustomerId': { _: CUSTOMER_ID},
                'ns:InternalReference': { _: orderNumber},
                'ns:Type': { _: ORDER_TYPE},
              }
            },
            'ns:clientContext': {
              'ns:DatabaseId': { _: DATABASE_ID }
            },
          }
        }
    };
    return requestObj;
  },
  //END INVOICE UPDATE REQUEST

  cancelOrder: function(requestData) {

    requestObject['soapenv:Envelope'][
      'soapenv:Body'
    ] = this.returnCancelOrderRequestXML(requestData);
    var xml = builder.buildObject(requestObject);

    requestHeader['SOAPAction'] = ACTION_URL + '/Cancel';
    return fetch(BASE_URL, {
      method: 'POST',
      headers: requestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
    });
  }, //end of cancelOrder

  returnCancelOrderRequestXML: function(
    { orderNumber, reasonId, customerId }
  ) {
    let requestObj = {
      'ns:Cancel': {
        'ns:cancelRequest': {
          'ns:Identifier': {
            'ns:Reference': {
              'ns:CustomerId': { _: CUSTOMER_ID },
              'ns:InternalReference': { _: orderNumber },
              'ns:Type': { _: ORDER_TYPE }
             },
          },
          'ns:ReasonId': { _: CANCELLATION_ID },
        },
        'ns:clientContext': {
          'ns:DatabaseId': { _: DATABASE_ID }
        }
      }
    };

    return requestObj;
  },

  generateReplacement: function(requestData) {
    requestObject['soapenv:Envelope'][
      'soapenv:Body'
    ] = salesHelper.returnCreateReplacementRequestXML(requestData);

    var xml = builder.buildObject(requestObject);
    LogHelper.logResponse("Generate replace for order number " + requestData.orderNumber + "\n" + xml);
    requestHeader['SOAPAction'] = CREATE_ACTION;
    return fetch(BASE_URL, {
      method: 'POST',
      headers: requestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
    });
  }, //end of generateReplacement

  callHelloWorld: function() {
    requestObject['soapenv:Envelope']['soapenv:Body'] = {
      'ns:HelloWorld': {
        'ns:text': { _: 'test text' },
        'ns:clientContext': {
          'ns:DatabaseId': { _: 'GULFTALEED_TEST' }
        }
      }
    };
    var xml = builder.buildObject(requestObject);
    let helloRequestHeader = {
      'Content-Type': 'text/xml; charset=utf-8',
      Authorization: AUTH_CODE,
      SOAPAction:
        'http://www.cegid.fr/Retail/1.0/ICbrBasicWebServiceInterface/HelloWorld'
    };
    return fetch(BASE_URL, {
      method: 'POST',
      headers: helloRequestHeader,
      body: xml
    }).then(response => {
      return response.text().then(data => ({
        data: data,
        status: response.status
      }));
      //return response.text();
    });
  }
};
