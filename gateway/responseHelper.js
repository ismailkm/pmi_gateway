var xml2js = require('xml2js');

module.exports = {
  helloWorldResponse: function(response) {
    let responseObj = [];
    xml2js.parseString(response, { trim: true }, function(err, result) {
      responseObj =
        result['s:Envelope']['s:Body'][0]['HelloWorldResponse'][0][
          'HelloWorldResult'
        ][0];
    });
    return responseObj;
  },

  createInvoiceResponse: function(response) {
    let responseObj = [];
    xml2js.parseString(response, { trim: true }, function(err, result) {
      responseObj =
        result['s:Envelope']['s:Body'][0]['CreateResponse'][0][
          'CreateResult'
        ][0];
    });
    return responseObj;
  },

  createErrorResponse: function(response) {
    let responseObj = [];
    xml2js.parseString(response, { trim: true }, function(err, result) {
      responseObj = result['s:Envelope']['s:Body'][0]['s:Fault'][0];
    });
    let message = '';
    if ('detail' in responseObj) {
      message =
        responseObj['detail'][0]['CbpExceptionDetail'][0]['InnerException'][0][
          'InnerException'
        ][0]['InnerException'][0]['Message'][0];
    } else {
      message = responseObj['faultstring'][0]['_'];
    }
    return message;
  }
};
