const fetch = require('node-fetch');
let LogHelper = require('../helpers/logHelper');
let AramexHelper = require('../helpers/aramexHelper');

var _ = require('lodash');
const { ARAMEX: { BASE_URL, SSA_LOGIN, SSA_PASSWORD, FACILITY, STORE_KEY } } = require('../constants');

module.exports = {

  importASN: function(transaction, allSales) {

    let facilityCode = AramexHelper.mapFacilityCode(transaction.StorerKey);

    let requestData = {
      'ApplicationHeader':  {
          'RequestedDate': transaction.RequestedDate,
          'RequestedSystem': transaction.RequestedSystem,
          'TransactionID': transaction.TransactionID
      },
      'DataHeader': {
          'ClinetSystemRef': transaction.TransactionID,
          'Currency': transaction.Currency,
          'Facility': facilityCode.code,
          'StorerKey': STORE_KEY,
          'Notes': transaction.Notes
      },
      'DataLines': getDataLines(allSales, transaction),
      'SSA': {
          'SSA_Login': SSA_LOGIN,
          'SSA_Password': SSA_PASSWORD
      }
    }

    LogHelper.logResponse("Request for TransactionID " + transaction.TransactionID + "\n" + JSON.stringify(requestData));
    // fetch
    fetch(BASE_URL, {
      method: 'POST',
      headers: {
      'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestData),
    }).then(response => {
      if(response.status == 200){
        return response.json();
      }else{
        let error = new Error("API response error " + response.status);
        throw error;
      }
    }).then(res => {
      if(res.Status == 'SUCCESS'){
        LogHelper.logResponse("ASN created successfully. Reference# " + res.Reference);
      }else{
        LogHelper.logResponse("Error response from API . Reference# " + res.ErrorDescription);
      }
    }).catch(error => {
      LogHelper.logResponse("API call error " + error.message);
    });
    //end fetch
  },//importASN

}

const getDataLines = (allSales, currentSale) => {
  let lines = _.filter(allSales, function(o) { return o.TransactionID == currentSale.TransactionID });
  let dataLines = [];
  for(let line of lines ){
    dataLines.push({
      'ExternLineNo': line.ExternLineNo,
      'LinePO': line.LinePO,
      'Qty': line.Qty,
      'SKU': line.SKU,
      'UnitCost': line.UnitCost
    })
  }
  return dataLines;
}
