var mongoose = require('mongoose');
//schema
var sfInvoiceModel = mongoose.Schema({
    request: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});
// Export sfInvoiceModel
var SfInvoice = module.exports = mongoose.model('sfInvoices', sfInvoiceModel);
