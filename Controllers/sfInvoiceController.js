SfInvoice = require('../models/sfInvoiceModel');

// exports.add
// };

module.exports = {

  saveRequestData: function (requestParams) {
      var sfInvoice = new SfInvoice();
      sfInvoice.request = JSON.stringify(requestParams);
      return sfInvoice.save();
  },//saveRequestData

  getAllRequests: async function () {

      const allRecords = await SfInvoice.find({});
      return allRecords;
  },//getAllRequests

  removeRequest: async function (id) {
      SfInvoice.deleteOne({_id: id})
               .then(res => {
                 console.log("res");
                 console.log(res);
               }).catch(error => {
                 console.log("error");
                 console.log(error);
               });
  },//removeRequest

}
