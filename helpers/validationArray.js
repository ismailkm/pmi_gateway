const { check } = require('express-validator/check');

module.exports = {
    receivestockArray: [
      check('internal_reference')
        .not()
        .isEmpty()
        .withMessage('Please provide Internal Reference'),
      check('document_date')
        .not()
        .isEmpty()
        .withMessage('Please provide Document date'),
      check('receipt_date')
        .not()
        .isEmpty()
        .withMessage('Please provide Receipt date'),
      check('warehouse_code')
        .not()
        .isEmpty()
        .withMessage('Please provide Warehouse code'),
      check('store_code')
        .not()
        .isEmpty()
        .withMessage('Please provide store code'),
      check('items.*.quantity')
        .not()
        .isEmpty()
        .withMessage('Please provide Quantity')
        .isInt()
        .withMessage('Quantity can only be number'),
      check('items.*.barcode')
        .not()
        .isEmpty()
        .withMessage('Please provide barcode'),
      check('items.*.codentifier')
        .not()
        .withMessage('Codentifier parameter is missing.')

    ],

    generateTransactionValidation:[
      check('firstName')
        .not()
        .isEmpty()
        .withMessage('Please provide first name'),
      check('lastName')
        .not()
        .isEmpty()
        .withMessage('Please provide last name'),
      check('items')
          .not()
          .isEmpty()
          .withMessage('Please provide last name'),
      check('orderNumber')
        .not()
        .isEmpty()
        .withMessage('Please provide order number'),
      check('orderDate')
        .not()
        .isEmpty()
        .withMessage('Please provide order date'),
      check('orderType')
        .not()
        .isEmpty()
        .withMessage('Please provide order type'),
      check('warehouseCode')
        .not()
        .isEmpty()
        .withMessage('Please provide warehouse code'),
      check('orderType')
        .not()
        .isEmpty()
        .withMessage('Please provide order type'),
     check('totalAmount')
          .not()
          .isEmpty()
          .withMessage('Please provide total amount')
          .isFloat()
          .withMessage('Total amount can only be number'),
      check('paymentType')
        .not()
        .isEmpty()
        .withMessage('Please provide payment type'),
      check('items.*.quantity')
        .not()
        .isEmpty()
        .withMessage('Please provide Quantity')
        .isInt()
        .withMessage('Quantity can only be number'),
      check('items.*.sku')
        .not()
        .isEmpty()
        .withMessage('Please provide SKU number'),
      check('items.*.originalPrice')
        .not()
        .isEmpty()
        .withMessage('Please provide Original Price')
        .isFloat()
        .withMessage('Original price can only be number'),
      check('items.*.finalPrice')
        .not()
        .isEmpty()
        .withMessage('Please provide Final Price')
        .isFloat()
        .withMessage('Final price can only be number'),
    ],

    generateReplacementValidation:[
      check('firstName')
        .not()
        .isEmpty()
        .withMessage('Please provide first name'),
      check('lastName')
        .not()
        .isEmpty()
        .withMessage('Please provide last name'),
      check('items')
          .not()
          .isEmpty()
          .withMessage('Please provide last name'),
      check('orderNumber')
        .not()
        .isEmpty()
        .withMessage('Please provide order number'),
      check('orderDate')
        .not()
        .isEmpty()
        .withMessage('Please provide order date'),
     check('totalAmount')
          .not()
          .isEmpty()
          .withMessage('Please provide total amount')
          .isFloat()
          .withMessage('Total amount can only be number'),
      check('paymentType')
        .not()
        .isEmpty()
        .withMessage('Please provide payment type'),
      check('items.*.quantity')
        .not()
        .isEmpty()
        .withMessage('Please provide Quantity')
        .isInt()
        .withMessage('Quantity can only be number'),
      check('items.*.sku')
        .not()
        .isEmpty()
        .withMessage('Please provide SKU number'),
      check('items.*.originalPrice')
        .not()
        .isEmpty()
        .withMessage('Please provide Original Price')
        .isFloat()
        .withMessage('Original price can only be number'),
      check('items.*.finalPrice')
        .not()
        .isEmpty()
        .withMessage('Please provide Final Price')
        .isFloat()
        .withMessage('Final price can only be number'),
      check('items.*.warehouseCode')
        .not()
        .isEmpty()
        .withMessage('Please provide warehouse code.')
    ],

    updateTransactionValidation: [
      check('orderNumber')
        .not()
        .isEmpty()
        .withMessage('Please provide order number'),
      check('invoiceNumber')
        .not()
        .isEmpty()
        .withMessage('Please provide invoice number'),
      check('warehouseCode')
        .not()
        .isEmpty()
        .withMessage('Please provide warehouse code'),
      check('orderDate')
        .not()
        .isEmpty()
        .withMessage('Please provide order date'),
    ],

    updatePaymentValidation: [
      check('orderNumber')
        .not()
        .isEmpty()
        .withMessage('Please provide order number'),
     check('totalAmount')
          .not()
          .isEmpty()
          .withMessage('Please provide total amount')
          .isFloat()
          .withMessage('Total amount can only be number'),
     check('paymentType')
          .not()
          .isEmpty()
          .withMessage('Please provide payment type'),
      check('paid')
          .not()
          .isEmpty()
          .withMessage('Please provide value of Paid')
          .isBoolean()
          .withMessage('Paid value can only either true or false.'),
    ]

};
