const fs = require("fs");
const csv = require('csv-parser');
var iconv = require('iconv-lite');
var xml2js = require('xml2js');

const { INVOICES_PUT_PATH } = require('../constants');
var PromisesHelper = function () { };

//
PromisesHelper.prototype.readCSVFile = function (fileName) {
  return new Promise((resolve, reject) => {
    const data = [];
    var readable = fs.createReadStream(fileName);

    readable.on('error', (err) => {
      reject(err);
    });

    // pipe function returns different stream
    readable
      .pipe(iconv.decodeStream('utf16le'))
      .pipe(csv({ separator: ';' }))
      .on('data', (row) => {
        data.push(row);
      })
      .on('error', e => {
        reject(e);
      })
      .on('end', () => {
        try {
          fs.unlinkSync(fileName)
          console.error("file deleted")
        } catch(err) {
          console.error(err)
        }
        resolve(data);
      });
  });

};//readCSVFile

PromisesHelper.prototype.getAllXmlFiles = function () {
  return new Promise((resolve, reject) => {

    const files = fs.readdirSync(INVOICES_PUT_PATH).filter(fn => fn.endsWith('.xml'));

    if(files.length === 0){
      reject(CustomException("no xml found in folder.", 404));
    }

    resolve(files);
  });
};//getAllXmlFiles

PromisesHelper.prototype.fileExists = function (fileName) {
  return new Promise((resolve, reject) => {
      fs.access(fileName, fs.constants.F_OK, (err) => {
           err ? resolve(false) : resolve(true)
       });
  });

};//readCSVFile

PromisesHelper.prototype.readXmlFile = async function(fileName) {
  return new Promise((resolve, reject) => {
    let parser = new xml2js.Parser();
    let xml_string = fs.readFileSync(fileName);
    parser.parseString(xml_string, function(error, result) {
        if(error === null) {
           let sender = result.message.header[0].Sender[0];
           let recipient = result.message.header[0].Recipient[0];
           let subject = result.message.header[0].Subject[0];
           let emailBody = result.message.header[0].EMailBody[0];
           resolve({sender, recipient, subject, emailBody});
        }
        else {
          reject(error);
        }
    });//parseString
  });//Promise

};

PromisesHelper.prototype.deleteFile = function(fileName) {
  try {
    fs.unlinkSync(fileName)
    //console.error("file deleted")
  } catch(err) {
    console.error(err)
  }
};

function CustomException(message, code) {
  const error = new Error(message);

  error.code = code;
  return error;
}

module.exports = new PromisesHelper();
