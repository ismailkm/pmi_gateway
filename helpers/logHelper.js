var moment = require('moment');
const fs = require("fs");
let { LOG_PATH } = require('../constants');

var LogHelper = function() {};

//
LogHelper.prototype.logResponse = function(string) {
  let today = moment().format("DD-MM-YYYY");
  let logfile = LOG_PATH+'aramex_request_'+today+'.log';
  let time = moment().format("DD-MM-YYYY HH:mm:ss");
  fs.appendFile(logfile, "["+ time +"] - " + string + "\n", function (err) {
    if (err) throw err;
    //console.log('File is created successfully.');
  });
};



module.exports = new LogHelper();
