var moment = require('moment');
var _ = require('lodash');
const fs = require("fs");
const { base64encode } = require('nodejs-base64');
const { CEGID: { CLIENT_URL, BASE_URL, DATABASE_ID, DOMAIN, USER, PASSWORD,
                STORE_ID, CUSTOMER_ID, ORIGIN, ORDER_TYPE, CURRENCY_ID,
                CANCELLATION_ID, SHIPPING_ID, INVOICE_FILE_NAME },
        REPLACEMENT_PREFIX, RETURN_PREFIX,
        REPLACEMENT, SALE_TYPE, WAREHOUSE_CODES } = require('../constants');

const AUTH_CODE = 'Basic ' + base64encode(DOMAIN + '\\'+ USER +':'+ PASSWORD); // user name Y2_DEMO\ENG
const ACTION_URL = CLIENT_URL + '/ISaleDocumentService';

let requestObject = {
  'soapenv:Envelope': {
    $: {
      'xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
      'xmlns:ns': CLIENT_URL
    },
    'soapenv:Header': {},
    'soapenv:Body': {}
  }
};

var SalesHelper = function() {};

//
SalesHelper.prototype.generateFileName = function(filePrefix, invoiceNumber, date) {
  return INVOICE_FILE_NAME + filePrefix +"_"+
  invoiceNumber +
  '_' +
  date +
  '.pdf'
};

SalesHelper.prototype.isValidReplacement = function(req) {
  return req.orderNumber.includes(REPLACEMENT_PREFIX, 0) ;
};

SalesHelper.prototype.isReturnOrder = function(req) {
  if(req.orderType === SALE_TYPE){
    let isReturn = true;

    _.forEach(req.items, function(item) {
      if(item.quantity > 0){
        isReturn = false;
        return false;
      }
    });
    return isReturn;
  }
  return false
};

SalesHelper.prototype.isValidReturnOrderNumber = function(orderNumber) {
    return orderNumber.includes(RETURN_PREFIX, 0) ;
};

SalesHelper.prototype.isPOReceiveStock = function(reference) {
    return reference.includes('PO', 0) ;
};

SalesHelper.prototype.checkReturnPricesAreValid = function(req) {

   if(req.totalAmount <= 0){
     return false;
   }

   let isValid = true;
   _.forEach(req.items, function(item) {
     if(item.originalPrice <= 0 || item.finalPrice <= 0){
       isValid = false;
       return false;
     }
   });

   return isValid;
};

SalesHelper.prototype.mapWarehouseCode = function(warehouse_code){
  let aramex_wh = _.findLast(WAREHOUSE_CODES, ['aramex_code', warehouse_code]);
  if(aramex_wh){
    return aramex_wh;
  }
  return false;
}

SalesHelper.prototype.returnInvoiceFilePrefix = function(warehouse_code){
  let aramex_wh = _.findLast(WAREHOUSE_CODES, ['cegid_code', warehouse_code]);
  return aramex_wh.file_prefix;
}

SalesHelper.prototype.returnCreateInvioceRequestXML = function(
  { firstName, lastName, orderNumber, orderType,
    paymentType, totalAmount, shippingCost, warehouseCode, items, orderDate,
    cegidStoreCode, cegidOrderOrigin, cegidOrderType
   },
  today
 ){
  let requestObj = {
    'ns:Create': {
      'ns:createRequest': {
        'ns:DeliveryAddress': {
          'ns:FirstName': { _: firstName },
          'ns:LastName': { _: lastName }
        },
        'ns:Header': {
          'ns:Active': { _: 1 },
          'ns:Comment': { _: orderType },
          'ns:CurrencyId': { _: CURRENCY_ID },
          'ns:CustomerId': { _: CUSTOMER_ID },
          'ns:Date': { _: orderDate },
          'ns:ExternalReference': { _: orderNumber },
          'ns:InternalReference': { _: orderNumber },
          'ns:OmniChannel': {
              'ns:BillingStatus': { _: 'Totally' },
              'ns:DeliveryStoreId': { _: warehouseCode },
              'ns:DeliveryType': { _: 'ShipByCentral' },
              'ns:FollowUpStatus': { _: 'Validated' },
              'ns:PaymentMethodId': { _: paymentType },
              'ns:PaymentStatus': { _: 'Pending' },
              'ns:ReturnStatus': { _: 'NotReturned' },
              'ns:ShippingStatus': { _: 'InProgress' },
          },
          'ns:Origin': { _: cegidOrderOrigin },
          'ns:StoreId': { _: cegidStoreCode },
          'ns:Type': { _: cegidOrderType },
          'ns:WarehouseId': { _: warehouseCode }
        },
        'ns:Lines': {
          'ns:Create_Line':[]
        },
        'ns:Payments': {
          'ns:Create_Payment': {
            'ns:Amount': { _: totalAmount },
            'ns:CurrencyId': { _: CURRENCY_ID },
            'ns:DueDate': { _: orderDate },
            'ns:Id': { _: 1 },
            'ns:MethodId': { _: paymentType }
         }
        }
      },
      'ns:clientContext': {
        'ns:DatabaseId': { _: DATABASE_ID }
      }
    }
  };

  let lineItems = [];
  for(let item of items){
    lineItems.push({
      'ns:Comment': { _: item.promotionId },
      'ns:ItemIdentifier': {
        'ns:Reference': { _: item.sku } //sku
      },
      'ns:NetUnitPrice': { _: item.finalPrice },
      'ns:Origin': { _: cegidOrderOrigin },
      'ns:Quantity': { _: item.quantity },
      'ns:SerialNumberId': { _: item.codentifier },
      'ns:UnitPrice': { _: item.originalPrice }
    });
  }
  requestObj['ns:Create']['ns:createRequest']['ns:Lines']['ns:Create_Line'] = lineItems;
  return requestObj;
}, //returnCreateInvioceRequestXML

SalesHelper.prototype.returnCreateReplacementRequestXML = function(
    { firstName, lastName, orderNumber, orderType,
      paymentType, totalAmount, shippingCost, items, orderDate },
    today
   ){
    let requestObj = {
      'ns:Create': {
        'ns:createRequest': {
          'ns:DeliveryAddress': {
            'ns:FirstName': { _: firstName },
            'ns:LastName': { _: lastName }
          },
          'ns:Header': {
            'ns:Active': { _: 1 },
            'ns:Comment': { _: REPLACEMENT },
            'ns:CurrencyId': { _: CURRENCY_ID },
            'ns:CustomerId': { _: CUSTOMER_ID },
            'ns:Date': { _: orderDate },
            'ns:ExternalReference': { _: orderNumber },
            'ns:InternalReference': { _: orderNumber },
            'ns:OmniChannel': {
                'ns:BillingStatus': { _: 'Totally' },
                'ns:DeliveryStoreId': { _: STORE_ID },
                'ns:DeliveryType': { _: 'ShipByCentral' },
                'ns:FollowUpStatus': { _: 'Validated' },
                'ns:PaymentMethodId': { _: paymentType },
                'ns:PaymentStatus': { _: 'Pending' },
                'ns:ReturnStatus': { _: 'NotReturned' },
                'ns:ShippingStatus': { _: 'InProgress' },
            },
            'ns:Origin': { _: ORIGIN },
            'ns:StoreId': { _: STORE_ID },
            'ns:Type': { _: ORDER_TYPE }
          },
          'ns:Lines': {
            'ns:Create_Line':[]
          },
          'ns:Payments': {
            'ns:Create_Payment': {
              'ns:Amount': { _: totalAmount },
              'ns:CurrencyId': { _: CURRENCY_ID },
              'ns:DueDate': { _: orderDate },
              'ns:Id': { _: 1 },
              'ns:MethodId': { _: paymentType }
           }
          }
        },
        'ns:clientContext': {
          'ns:DatabaseId': { _: DATABASE_ID }
        }
      }
    };

    let lineItems = [];
    for(let item of items){
      lineItems.push({
        'ns:Comment': { _: item.promotionId },
        'ns:ItemIdentifier': {
          'ns:Reference': { _: item.sku } //sku
        },
        'ns:OmniChannel': {
          'ns:WarehouseId': { _: item.warehouseCode }
        },
        'ns:NetUnitPrice': { _: item.finalPrice },
        'ns:Origin': { _: ORIGIN },
        'ns:Quantity': { _: item.quantity },
        'ns:SerialNumberId': { _: item.codentifier },
        'ns:UnitPrice': { _: item.originalPrice }
      });
    }
    requestObj['ns:Create']['ns:createRequest']['ns:Lines']['ns:Create_Line'] = lineItems;
    return requestObj;
  }, //returnCreateReplacementRequestXML

module.exports = new SalesHelper();
