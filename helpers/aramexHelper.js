var _ = require('lodash');

const { FACILITY_CODE } = require('../constants');

var AramexHelper = function() {};

AramexHelper.prototype.mapFacilityCode = function(store_key){
  let aramex_wh = _.findLast(FACILITY_CODE, ['store_key', store_key]);
  if(aramex_wh){
    return aramex_wh;
  }
  return false;
}


module.exports = new AramexHelper();
